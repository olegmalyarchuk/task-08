package com.epam.rd.java.basic.task8;

import java.util.Objects;
import javax.xml.*;

public class Flower {
    private String name;
    private String soil;
    private String origin;

    private String stemColour;
    private String leafColour;
    private int aveLenFlower;

    private int temperature;
    private boolean lightRequiring;
    private int watering;

    private String multiplying;

    public String getName() {
        return name;
    }
    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public boolean isLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(boolean lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", temperature=" + temperature +
                ", lightRequiring=" + lightRequiring +
                ", watering=" + watering +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return aveLenFlower == flower.aveLenFlower && temperature == flower.temperature && lightRequiring == flower.lightRequiring && watering == flower.watering && Objects.equals(name, flower.name) && Objects.equals(soil, flower.soil) && Objects.equals(origin, flower.origin) && Objects.equals(stemColour, flower.stemColour) && Objects.equals(leafColour, flower.leafColour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, soil, origin, stemColour, leafColour, aveLenFlower, temperature, lightRequiring, watering);
    }
}
