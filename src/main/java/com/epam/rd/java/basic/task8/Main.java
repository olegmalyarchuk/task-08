package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
//		if (args.length != 1) {
//			return;
//		}
		
		//String xmlFileName = args[0];
		String xmlFileName = "input.xml";
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		ArrayList<Flower> flowers = new ArrayList<>();

		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(xmlFileName));

		NodeList nodeList = document.getDocumentElement().getElementsByTagName("flower");
		List<Flower> flowerList = new ArrayList<>();
		for(int i = 0; i < nodeList.getLength(); i++) {
			flowerList.add(domController.getFlowerr(nodeList.item(i)));
		}


		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
		document = documentBuilder.newDocument();
		Element root = document.createElement("flowers");
		root.setAttribute("xmlns", "http://www.nure.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
		document.appendChild(root);
		Element element;
		for(Flower f : flowerList) {
			Element flower = document.createElement("flower");
			element = document.createElement("name");
			element.setTextContent(f.getName());
			flower.appendChild(element);
			element = document.createElement("soil");
			element.setTextContent(f.getSoil());
			flower.appendChild(element);
			element = document.createElement("origin");
			element.setTextContent(f.getOrigin());
			flower.appendChild(element);
				Element visualParameters = document.createElement("visualParameters");
				element = document.createElement("stemColour");
				element.setTextContent(f.getStemColour());
				visualParameters.appendChild(element);
				element = document.createElement("leafColour");
				element.setTextContent(f.getLeafColour());
				visualParameters.appendChild(element);
			flower.appendChild(visualParameters);
				Element growingTips = document.createElement("growingTips");
				element = document.createElement("tempreture");
				element.setAttribute("measure", "celcius");
				element.setTextContent(f.getTemperature()+"");
				growingTips.appendChild(element);
				element = document.createElement("lighting");
				element.setAttribute("lightRequiring", "yes");
				growingTips.appendChild(element);
				element = document.createElement("watering");
				element.setAttribute("measure", "mlPerWeek");
				element.setTextContent(f.getWatering()+"");
				growingTips.appendChild(element);
			flower.appendChild(growingTips);
			element = document.createElement("multiplying");
			element.setTextContent(f.getMultiplying());
			flower.appendChild(element);
			root.appendChild(flower);
		}

		StreamResult result =  new StreamResult(new File("output.dom.xml"));
		StreamResult result2 = new StreamResult(new File("output.sax.xml"));
		StreamResult result3 = new StreamResult(new File("output.stax.xml"));
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.transform(new DOMSource(document), result);
		t.transform(new DOMSource(document), result2);
		t.transform(new DOMSource(document), result3);


		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);

		String outputXmlFile = "output.sax.xml";



		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
	//	STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
	}

}
