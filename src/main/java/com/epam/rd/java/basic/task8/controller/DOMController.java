package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName){
		this.xmlFileName = xmlFileName;
	}

	public void printInfoAboutAllChildNodes(NodeList list) {
		for(int j = 0; j < list.getLength(); j++) {
			Node node = list.item(j);
			if(node.getNodeType() == Node.TEXT_NODE) {
				String textInformation = node.getNodeValue().replace("\n", "").trim();
				if(!textInformation.isEmpty()) {
					System.out.println("Всередині елемента знайдений текст: " + node.getNodeValue());
				}
			} else {
				System.out.println("Знайдений елемент: " + node.getNodeName() + ", його атрибути");
				NamedNodeMap attributes = node.getAttributes();
				for(int k = 0; k < attributes.getLength(); k++) {
//					System.out.print("Ім'я атрибуту: ");
//					System.out.println(attributes.item(k).getNodeName());
//					System.out.print(", його значення ");
//					System.out.println(attributes.item(k).getNodeValue());
				}
			}

			if(node.hasChildNodes()) {
				printInfoAboutAllChildNodes(node.getChildNodes());
			}
		}
	}

	public Flower getFlowerr(Node node) {
		Flower flower = new Flower();
		if(node.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) node;
			flower.setName(getTagValue("name", element));
			flower.setSoil(getTagValue("soil", element));
			flower.setOrigin(getTagValue("origin", element));
			flower.setStemColour(getTagValue("stemColour", element));
			flower.setLeafColour(getTagValue("leafColour", element));
			flower.setAveLenFlower(Integer.valueOf(getTagValue("aveLenFlower", element)));
			flower.setTemperature(Integer.valueOf(getTagValue("tempreture", element)));
			flower.setLightRequiring(true);
			flower.setWatering((Integer.valueOf(getTagValue("watering", element))));
			flower.setMultiplying(getTagValue("multiplying", element));
		}
		return flower;
	}

	public String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node)nodeList.item(0);
		return node.getNodeValue();
	}


	public void saveXML(List<Flower> flowers, String fileName) {
		//Document doc = new Document();
	}

}

